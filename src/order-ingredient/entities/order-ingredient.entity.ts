import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { OrderIngredientItem } from './orderItem-ingredient.entity';

@Entity()
export class OrderIngredient {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  createdDate: Date;

  @Column()
  totalBefore: number;

  //   @Column()
  //   memberDiscount: number;

  //จำนวนเงินสุทธิ
  @Column()
  total: number;

  //จำนวนชิ้นทั้งหมด
  @Column()
  totalAmount: number;

  //จำนวนเงินที่ได้รับ
  @Column()
  receivedAmount: number;

  @Column()
  change: number;

  @Column()
  paymentType: string;

  // user?: User;
  @OneToMany(
    () => OrderIngredientItem,
    (orderIngredientItem) => orderIngredientItem.orderIngredient,
  )
  orderIngredientItems?: OrderIngredientItem[];
}
