import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { OrderIngredient } from './order-ingredient.entity';
import { IngredientStore } from 'src/ingredient-store/entities/ingredient-store.entity';
@Entity()
export class OrderIngredientItem {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  price: number;
  @Column()
  acc: number;
  @Column()
  qty: number;
  @Column()
  unit: string;

  @ManyToOne(
    () => OrderIngredient,
    (orderIngredient) => orderIngredient.orderIngredientItems,
    { onDelete: 'CASCADE' },
  )
  orderIngredient: OrderIngredient;

  @ManyToOne(
    () => IngredientStore,
    (ingredientStore) => ingredientStore.orderIngredientItems,
  )
  ingredientStore: IngredientStore;
}
