import { Module } from '@nestjs/common';
import { OrderIngredientService } from './order-ingredient.service';
import { OrderIngredientController } from './order-ingredient.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderIngredient } from './entities/order-ingredient.entity';
import { OrderIngredientItem } from './entities/orderItem-ingredient.entity';
import { IngredientStore } from 'src/ingredient-store/entities/ingredient-store.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      OrderIngredient,
      OrderIngredientItem,
      IngredientStore,
    ]),
  ],
  controllers: [OrderIngredientController],
  providers: [OrderIngredientService],
})
export class OrderIngredientModule {}
