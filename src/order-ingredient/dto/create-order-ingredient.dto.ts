export class CreateOrderIngredientDto {
  orderIngredientItems: {
    ingredientId: number;
    qty: number;
  }[];
}
