import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrderIngredientService } from './order-ingredient.service';
import { CreateOrderIngredientDto } from './dto/create-order-ingredient.dto';
import { UpdateOrderIngredientDto } from './dto/update-order-ingredient.dto';

@Controller('order-ingredient')
export class OrderIngredientController {
  constructor(
    private readonly orderIngredientService: OrderIngredientService,
  ) {}

  @Post()
  create(@Body() createOrderIngredientDto: CreateOrderIngredientDto) {
    console.log('666');
    console.log(createOrderIngredientDto);
    return this.orderIngredientService.create(createOrderIngredientDto);
  }

  @Get()
  findAll() {
    return this.orderIngredientService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.orderIngredientService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateOrderIngredientDto: UpdateOrderIngredientDto,
  ) {
    return this.orderIngredientService.update(+id, updateOrderIngredientDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderIngredientService.remove(+id);
  }
}
