import { OrderIngredientItem } from 'src/order-ingredient/entities/orderItem-ingredient.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class IngredientStore {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  // inStock: number;
  // @Column()
  // maximum: number;
  // @Column()
  unit: string;
  @CreateDateColumn()
  createdDate: Date;

  @Column()
  price: number;
  @OneToMany(
    () => OrderIngredientItem,
    (orderingredientItem) => orderingredientItem.ingredientStore,
  )
  orderIngredientItems: OrderIngredientItem[];
}
