export class CreateIngredientStoreDto {
  name: string;
  inStock: number;
  maximum: number;
  unit: string;
  price: number;
  createdDate: Date;
  [key: string]: number | string | Date;
}
