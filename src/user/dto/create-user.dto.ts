export class CreateUserDto {
  email: string;

  password: string;

  fullName: string;

  tel: string;

  roles: string;

  image: string;
}
