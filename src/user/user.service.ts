import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from 'src/role/entities/role.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}
  create(createUserDto: CreateUserDto) {
    const user = new User();
    user.email = createUserDto.email;
    user.fullName = createUserDto.fullName;
    user.tel = createUserDto.tel;
    user.password = createUserDto.password;
    user.roles = JSON.parse(createUserDto.roles);
    if (createUserDto.image && createUserDto.image !== '') {
      user.image = createUserDto.image;
    }
    console.log(user);
    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find({ relations: { roles: true } });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOneOrFail({
      where: { email },
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = new User();
    user.email = updateUserDto.email;
    user.fullName = updateUserDto.fullName;
    user.tel = updateUserDto.tel;
    user.password = updateUserDto.password;
    user.roles = JSON.parse(updateUserDto.roles);
    if (updateUserDto.image && updateUserDto.image !== '') {
      user.image = updateUserDto.image;
    }
    const updateUser = await this.usersRepository.findOneOrFail({
      where: { id },
      relations: { roles: true },
    });
    updateUser.email = user.email;
    updateUser.fullName = user.fullName;
    updateUser.tel = user.tel;
    updateUser.password = user.password;
    updateUser.image = user.image;
    console.log('Userrr', user);
    console.log('Update Userrr', updateUser);
    for (const r of user.roles) {
      const searchRole = updateUser.roles.find((roles) => roles.id === r.id);
      if (!searchRole) {
        const newRole = await this.rolesRepository.findOneBy({ id: r.id });
        updateUser.roles.push(newRole);
      }
    }
    console.log('find old roles user ' + user.roles);
    console.log('find old roles update ' + updateUser.roles);

    for (let i = 0; i < updateUser.roles.length; i++) {
      const index = user.roles.findIndex(
        (role) => role.id === updateUser.roles[i].id,
      );
      if (index < 0) {
        updateUser.roles.splice(i, 1);
      }
    }
    console.log('find new roles ' + updateUser.roles);
    const result = await this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
    console.log(result);
    console.log(updateUser);
    return this.usersRepository.save(updateUser);
  }

  async remove(id: number) {
    const deleteProduct = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    await this.usersRepository.remove(deleteProduct);

    return deleteProduct;
  }
}
