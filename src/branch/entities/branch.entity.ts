import { Status } from 'src/status/entities/status.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  address: string;

  @ManyToOne(() => Status, (status) => status.branch, { onDelete: 'CASCADE' })
  status: Status;
}
