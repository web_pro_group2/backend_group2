export class CreateBranchDto {
  address: string;
  status: { id: number; name: string };
}
