import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { Branch } from './entities/branch.entity';
import { InjectRepository } from '@nestjs/Typeorm';
import { Repository } from 'Typeorm';

@Injectable()
export class BranchService {
  constructor(
    @InjectRepository(Branch) private branchsRepository: Repository<Branch>,
  ) {}
  create(createBranchDto: CreateBranchDto) {
    return this.branchsRepository.save(createBranchDto);
  }

  findAll() {
    return this.branchsRepository.find({ relations: { status: true } });
  }

  findOne(id: number) {
    return this.branchsRepository.findOne({
      where: { id },
      relations: { status: true },
    });
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    const updateBranch = await this.branchsRepository.findOneOrFail({
      where: { id },
      relations: { status: true },
    });
    await this.branchsRepository.update(id, {
      ...updateBranch,
      ...updateBranchDto,
    });
    const result = await this.branchsRepository.findOne({
      where: { id },
      relations: { status: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteBranch = await this.branchsRepository.findOneOrFail({
      where: { id },
    });
    await this.branchsRepository.remove(deleteBranch);
    return deleteBranch;
  }
}
