import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { IngredientModule } from './ingredient/ingredient.module';
import { PromotionModule } from './promotion/promotion.module';
import { MemberModule } from './member/member.module';
import { Member } from './member/entities/member.entity';
import { BranchModule } from './branch/branch.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Branch } from './branch/entities/branch.entity';
import { Ingredient } from './ingredient/entities/ingredient.entity';
import { DataSource } from 'typeorm';
import { Promotion } from './promotion/entities/promotion.entity';
import { TypeModule } from './type/type.module';
import { Product } from './product/entities/product.entity';
import { Type } from './type/entities/type.entity';
import { ProductModule } from './product/product.module';
import { UserModule } from './user/user.module';
import { User } from './user/entities/user.entity';
import { Status } from './status/entities/status.entity';
import { StatusModule } from './status/status.module';
import { HistoryModule } from './history/history.module';
import { History } from './history/entities/history.entity';
import { OrderModule } from './order/order.module';
import { OrderItem } from './order/entities/orderItem.entity';
import { Order } from './order/entities/order.entity';
import { join } from 'path';
import { ServeStaticModule } from '@nestjs/serve-static';
import { OrderIngredient } from './order-ingredient/entities/order-ingredient.entity';
import { OrderIngredientItem } from './order-ingredient/entities/orderItem-ingredient.entity';
import { IngredientStore } from './ingredient-store/entities/ingredient-store.entity';
import { OrderIngredientModule } from './order-ingredient/order-ingredient.module';
import { IngredientStoreModule } from './ingredient-store/ingredient-store.module';
import { AuthModule } from './auth/auth.module';
import { Role } from './role/entities/role.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'Work_group2_DB.sqlite',
      synchronize: true,
      entities: [
        Branch,
        Ingredient,
        Promotion,
        User,
        Member,
        Order,
        OrderItem,
        Product,
        Type,
        Status,
        History,
        OrderIngredient,
        OrderIngredientItem,
        IngredientStore,
        Role,
      ],
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    IngredientModule,
    PromotionModule,
    MemberModule,
    BranchModule,
    TypeModule,
    ProductModule,
    UserModule,
    StatusModule,
    HistoryModule,
    OrderModule,
    OrderIngredientModule,
    IngredientStoreModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
