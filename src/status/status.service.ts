import { Injectable } from '@nestjs/common';
import { CreateStatusDto } from './dto/create-status.dto';
import { UpdateStatusDto } from './dto/update-status.dto';
import { Status } from './entities/status.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class StatusService {
  constructor(
    @InjectRepository(Status)
    private statusRepository: Repository<Status>,
  ) {}
  create(createStatusDto: CreateStatusDto) {
    return this.statusRepository.save(createStatusDto);
  }

  findAll() {
    return this.statusRepository.find();
  }

  findOne(id: number) {
    return this.statusRepository.findOneBy({ id });
  }

  async update(id: number, updateStatusDto: UpdateStatusDto) {
    await this.statusRepository.update(id, updateStatusDto);
    const Status = await this.statusRepository.findOneBy({ id });
    return Status;
  }

  async remove(id: number) {
    const deleteStatus = await this.statusRepository.findOneBy({ id });
    return this.statusRepository.remove(deleteStatus);
  }
}
