import { Branch } from 'src/branch/entities/branch.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Status {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 'On Hold' })
  name: string;

  @OneToMany(() => Branch, (branch) => branch.status)
  branch: Branch[];
}
