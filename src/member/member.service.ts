import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/member.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MemberService {
  lastId: number = 5;
  member: Member[] = [
    {
      id: 1,
      name: 'Kurt Cobain',
      tel: '0912345678',
      createdDate: new Date('01/01/2024'),
    },
    {
      id: 2,
      name: 'Axl Rose',
      tel: '0911222266',
      createdDate: new Date('01/01/2024'),
    },
    {
      id: 3,
      name: 'Freddie Mercury',
      tel: '0913332222',
      createdDate: new Date('01/01/2024'),
    },
    {
      id: 4,
      name: 'Jimi Hendrix',
      tel: '0811212252',
      createdDate: new Date('01/01/2024'),
    },
    {
      id: 5,
      name: 'Anthony Kiedis',
      tel: '0875556666',
      createdDate: new Date('01/01/2024'),
    },
  ];
  constructor(
    @InjectRepository(Member)
    private MemberRepository: Repository<Member>,
  ) {}
  create(createMemberDto: CreateMemberDto) {
    const newMember = { ...createMemberDto, id: ++this.lastId };
    return this.MemberRepository.save(newMember);
  }

  findAll() {
    return this.MemberRepository.find();
  }

  findOne(id: number) {
    return this.MemberRepository.findOneBy({ id });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    await this.MemberRepository.update(id, updateMemberDto);
    const Member = await this.MemberRepository.findOneBy({ id });
    return Member;
  }

  async remove(id: number) {
    const deleteMember = await this.MemberRepository.findOneBy({ id });
    return this.MemberRepository.remove(deleteMember);
  }
}
