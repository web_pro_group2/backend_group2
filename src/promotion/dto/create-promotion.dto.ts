export class CreatePromotionDto {
  condition: string;
  startDate: Date;
  endDate: Date;
  [key: string]: number | string | Date;
}
