import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Promotion } from './entities/promotion.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PromotionService {
  lastId: number = 2;
  promotion: Promotion[] = [
    {
      id: 1,
      condition: 'ซื้อครบ 200 บาท',
      startDate: new Date('01/01/2024'),
      endDate: new Date('01/01/2024'),
    },
    {
      id: 2,
      condition: 'ซื้อครบ 200 บาท',
      startDate: new Date('01/01/2024'),
      endDate: new Date('01/01/2024'),
    },
  ];
  constructor(
    @InjectRepository(Promotion)
    private PromotionRepository: Repository<Promotion>,
  ) {}
  create(createPromotionDto: CreatePromotionDto) {
    const newPromotion = { ...createPromotionDto, id: ++this.lastId };
    return this.PromotionRepository.save(newPromotion);
  }

  findAll() {
    return this.PromotionRepository.find();
  }

  findOne(id: number) {
    return this.PromotionRepository.findOneBy({ id });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    await this.PromotionRepository.update(id, updatePromotionDto);
    const Promotion = await this.PromotionRepository.findOneBy({ id });
    return Promotion;
  }

  async remove(id: number) {
    const deletePromotion = await this.PromotionRepository.findOneBy({ id });
    return this.PromotionRepository.remove(deletePromotion);
  }
}
