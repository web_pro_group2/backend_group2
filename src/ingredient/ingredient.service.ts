import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateIngredientDto } from './dto/create-ingredient.dto';
import { UpdateIngredientDto } from './dto/update-ingredient.dto';
import { Ingredient } from './entities/ingredient.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { History } from 'src/history/entities/history.entity';

@Injectable()
export class IngredientService {
  lastId: number = 0;

  constructor(
    @InjectRepository(History)
    private HistoryRepository: Repository<History>, // Inject History repository
    @InjectRepository(Ingredient)
    private IngredientRepository: Repository<Ingredient>,
  ) {}
  async create(createIngredientDto: CreateIngredientDto) {
    const newIngredient = { ...createIngredientDto, id: ++this.lastId };
    const savedIngredient = await this.IngredientRepository.save(newIngredient);

    // Create a new History entry for the saved Ingredient
    // const newHistory = new History();
    // newHistory.idIngredient = new Number();
    // newHistory.name = new Date();
    // newHistory.inStock = new Date();
    // newHistory.Maximum = new Date();
    // newHistory.Unit = new Date();
    // newHistory.createdate = new Date();
    // newHistory.Ingredients = [savedIngredient];

    const newHistory = new History();
    newHistory.idIngredient = savedIngredient.id;
    newHistory.name = savedIngredient.name;
    newHistory.inStock = savedIngredient.inStock;
    newHistory.Maximum = savedIngredient.Maximum;
    newHistory.Unit = savedIngredient.Unit;
    newHistory.createdDate = new Date();
    newHistory.Ingredients = [savedIngredient];
    await this.HistoryRepository.save(newHistory);

    return savedIngredient;
  }

  findAll() {
    return this.IngredientRepository.find({ relations: { history: true } });
  }

  findOne(id: number) {
    return this.IngredientRepository.findOne({
      where: { id },
      relations: { history: true },
    });
  }

  async update(id: number, updateIngredientDto: UpdateIngredientDto) {
    const updatedIngredient = await this.IngredientRepository.findOne({
      where: { id },
    });
    if (!updatedIngredient) {
      throw new NotFoundException(`Ingredient with id ${id} not found`);
    }

    // อัปเดตข้อมูลใน updatedIngredient ด้วยข้อมูลใหม่
    updatedIngredient.name = updateIngredientDto.name;
    updatedIngredient.inStock = updateIngredientDto.inStock;
    updatedIngredient.Maximum = updateIngredientDto.Maximum;
    updatedIngredient.Unit = updateIngredientDto.Unit;

    // บันทึกข้อมูลการอัปเดตลงในฐานข้อมูล
    await this.IngredientRepository.save(updatedIngredient);

    // สร้าง History สำหรับการอัปเดต Ingredient
    const newHistory = new History();
    newHistory.idIngredient = id;
    newHistory.name = updatedIngredient.name;
    newHistory.inStock = updatedIngredient.inStock;
    newHistory.Maximum = updatedIngredient.Maximum;
    newHistory.Unit = updatedIngredient.Unit;
    newHistory.createdDate = new Date();
    newHistory.Ingredients = [updatedIngredient];
    await this.HistoryRepository.save(newHistory);

    // ส่งข้อมูล Ingredient ที่อัปเดตแล้วกลับ
    return updatedIngredient;
  }

  // async update(id: number, updateIngredientDto: UpdateIngredientDto) {
  //   const updatedIngredient = await this.IngredientRepository.findOne({
  //     where: { id },
  //   });
  //   if (!updatedIngredient) {
  //     throw new NotFoundException(`Ingredient with id ${id} not found`);
  //   }

  //   // สร้าง History สำหรับการอัปเดต Ingredient
  //   const newHistory = new History();
  //   newHistory.idIngredient = id;
  //   newHistory.name = updatedIngredient.name;
  //   newHistory.inStock = updatedIngredient.inStock;
  //   newHistory.Maximum = updatedIngredient.Maximum;
  //   newHistory.Unit = updatedIngredient.Unit;
  //   newHistory.createdDate = new Date();
  //   newHistory.Ingredients = [updatedIngredient];
  //   await this.HistoryRepository.save(newHistory);

  //   // อัปเดต Ingredient
  //   await this.IngredientRepository.update(id, updateIngredientDto);

  //   return await this.IngredientRepository.findOne({ where: { id } });
  // }

  async remove(id: number) {
    const deleteIngredient = await this.IngredientRepository.findOneBy({ id });
    return this.IngredientRepository.remove(deleteIngredient);
  }
}
