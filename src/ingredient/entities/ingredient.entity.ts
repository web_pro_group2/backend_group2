import { History } from 'src/history/entities/history.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Ingredient {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  inStock: number;
  @Column()
  Maximum: number;
  @Column()
  Unit: string;
  @Column()
  createdDate: Date;

  // [key: string]: number | string | Date;

  @ManyToOne(() => History, (history) => history.Ingredients)
  history: History;
}
